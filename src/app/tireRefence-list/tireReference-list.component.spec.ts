import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TireReferenceListComponent } from './tireReference-list.component';

describe('TireReferenceListComponent', () => {
  let component: TireReferenceListComponent;
  let fixture: ComponentFixture<TireReferenceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TireReferenceListComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TireReferenceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
