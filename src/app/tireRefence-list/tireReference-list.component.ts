import { Component, OnInit } from '@angular/core';
import { TireReference } from '../tireReference';
import { Page } from '../pagination/page';
import { Pageable } from '../pagination/pageable';
import { CustomPaginationService } from '../pagination/services/custom-pagination.service';

import { TireReferenceService } from '../tireReference.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tireReference-list',
  templateUrl: './tireReference-list.component.html',
  styleUrls: ['./tireReference-list.component.css'],
})
export class TireReferenceListComponent implements OnInit {
  page: Page<TireReference> = new Page();
  name: string = '';

  constructor(
    private tireReferenceService: TireReferenceService,
    private paginationService: CustomPaginationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getTireReferences(this.name);
  }

  public getTireReferences(name: string) {
    this.tireReferenceService
      .seachTireReferences(name, this.page.pageable)
      .subscribe((data) => {
        this.page = data;
      });
  }

  public tireReferenceDetails(id: string) {
    this.router.navigate(['tireReference-details', id]);
  }

  public updateTireReference(id: string) {
    this.router.navigate(['update-tireReference', id]);
  }

  public deleteTireReference(id: string) {
    this.tireReferenceService.deleteTireReference(id).subscribe((data) => {
      console.log(data);
      this.getTireReferences(this.name);
    });
  }

  public getNextPage(): void {
    this.page.pageable = this.paginationService.getNextPage(this.page);
    this.getTireReferences(this.name);
  }

  public getPreviousPage(): void {
    this.page.pageable = this.paginationService.getPreviousPage(this.page);
    this.getTireReferences(this.name);
  }

  public getPageInNewSize(pageSize: number): void {
    this.page.pageable = this.paginationService.getPageInNewSize(
      this.page,
      pageSize
    );
    this.getTireReferences(this.name);
  }
}
