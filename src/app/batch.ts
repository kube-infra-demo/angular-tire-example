import { v4 as uuid } from 'uuid';
export class Batch {
  id: string = uuid();
  Date: Date;
  FactoryLocation: string;
}
