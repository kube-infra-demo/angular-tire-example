import { TestBed } from '@angular/core/testing';

import { TireReferenceService } from './tireReference.service';

describe('TireReferenceService', () => {
  let service: TireReferenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TireReferenceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
