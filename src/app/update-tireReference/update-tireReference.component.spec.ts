import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateTireReferenceComponent } from './update-tireReference.component';

describe('UpdateTireReferenceComponent', () => {
  let component: UpdateTireReferenceComponent;
  let fixture: ComponentFixture<UpdateTireReferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateTireReferenceComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateTireReferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
