import { Component, OnInit } from '@angular/core';
import { TireReferenceService } from '../tireReference.service';
import { TireReference } from '../tireReference';
import { ActivatedRoute, Router } from '@angular/router';
import { Batch } from '../Batch';

@Component({
  selector: 'app-update-tireReference',
  templateUrl: './update-tireReference.component.html',
  styleUrls: ['./update-tireReference.component.css'],
})
export class UpdateTireReferenceComponent implements OnInit {
  id: string;
  tireReference: TireReference = new TireReference();
  constructor(
    private tireReferenceService: TireReferenceService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.tireReferenceService.getTireReferenceById(this.id).subscribe(
      (data) => {
        this.tireReference = data;
      },
      (error) => console.log(error)
    );
  }

  onSubmit() {
    this.tireReferenceService
      .updateTireReference(this.id, this.tireReference)
      .subscribe(
        (data) => {
          this.goToTireReferenceList();
        },
        (error) => console.log(error)
      );
  }

  goToTireReferenceList() {
    this.router.navigate(['/tireReferences']);
  }
  addNew() {
    this.tireReference.batches.push(new Batch());
    console.log(this.tireReference.batches);
  }

  remove(index: number) {
    this.tireReference.batches.splice(index, 1);
  }
}
