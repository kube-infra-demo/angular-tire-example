import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TireReferenceListComponent } from './tireRefence-list/tireReference-list.component';
import { CreateTireReferenceComponent } from './create-tireReference/create-tireReference.component';
import { FormsModule } from '@angular/forms';
import { UpdateTireReferenceComponent } from './update-tireReference/update-tireReference.component';
import { TireReferenceDetailsComponent } from './tireReference-details/tireReference-details.component';
import { CustomPaginationComponent } from './pagination/components/custom-pagination/custom-pagination.component';

@NgModule({
  declarations: [
    AppComponent,
    TireReferenceListComponent,
    CreateTireReferenceComponent,
    UpdateTireReferenceComponent,
    TireReferenceDetailsComponent,
    CustomPaginationComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
