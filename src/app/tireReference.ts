import { v4 as uuid } from 'uuid';
import { Batch } from './Batch';

export class TireReference {
  id: string = uuid();
  name: string;
  size: string;
  remainingQuantity: number;
  price: number;
  batches: Batch[] = [];
}
