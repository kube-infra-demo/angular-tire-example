import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TireReference } from './tireReference';
import { Page } from './pagination/page';
import { Pageable } from './pagination/pageable';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TireReferenceService {
  private baseURL = environment.backendUrl;

  constructor(private httpClient: HttpClient) {}

  seachTireReferences(
    name: string,
    pageable: Pageable
  ): Observable<Page<TireReference>> {
    let url = this.baseURL + '/search';
    let queryOpts =
      '?page=' +
      pageable.pageNumber +
      '&size=' +
      pageable.pageSize +
      '&sort=name';
    if (name != '') {
      queryOpts += '&name=' + name;
    }
    return this.httpClient.get<Page<TireReference>>(url + queryOpts);
  }

  createTireReference(tireReference: TireReference): Observable<Object> {
    return this.httpClient.post(`${this.baseURL}`, tireReference);
  }

  getTireReferenceById(id: string): Observable<TireReference> {
    return this.httpClient.get<TireReference>(`${this.baseURL}/${id}`);
  }

  updateTireReference(
    id: string,
    tireReference: TireReference
  ): Observable<Object> {
    return this.httpClient.put(`${this.baseURL}/${id}`, tireReference);
  }

  deleteTireReference(id: string): Observable<Object> {
    return this.httpClient.delete(`${this.baseURL}/${id}`);
  }
}
