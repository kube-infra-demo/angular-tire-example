import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TireReferenceDetailsComponent } from './tireReference-details.component';

describe('TireReferenceDetailsComponent', () => {
  let component: TireReferenceDetailsComponent;
  let fixture: ComponentFixture<TireReferenceDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TireReferenceDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TireReferenceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
