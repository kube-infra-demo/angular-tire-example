import { Component, OnInit } from '@angular/core';
import { TireReference } from '../tireReference';
import { ActivatedRoute } from '@angular/router';
import { TireReferenceService } from '../tireReference.service';

@Component({
  selector: 'app-tireReference-details',
  templateUrl: './tireReference-details.component.html',
  styleUrls: ['./tireReference-details.component.css'],
})
export class TireReferenceDetailsComponent implements OnInit {
  id: string;
  tireReference: TireReference;
  constructor(
    private route: ActivatedRoute,
    private employeService: TireReferenceService
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.tireReference = new TireReference();
    this.employeService.getTireReferenceById(this.id).subscribe((data) => {
      this.tireReference = data;
      console.log(this.tireReference);
    });
  }
}
