import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTireReferenceComponent } from './create-tireReference.component';

describe('CreateTireReferenceComponent', () => {
  let component: CreateTireReferenceComponent;
  let fixture: ComponentFixture<CreateTireReferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateTireReferenceComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTireReferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
