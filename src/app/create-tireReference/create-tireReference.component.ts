import { Component, OnInit } from '@angular/core';
import { TireReference } from '../tireReference';
import { TireReferenceService } from '../tireReference.service';
import { Router } from '@angular/router';
import { Batch } from '../Batch';

@Component({
  selector: 'app-create-tireReference',
  templateUrl: './create-tireReference.component.html',
  styleUrls: ['./create-tireReference.component.css'],
})
export class CreateTireReferenceComponent implements OnInit {
  tireReference: TireReference = new TireReference();
  constructor(
    private tireReferenceService: TireReferenceService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  saveTireReference() {
    this.tireReferenceService.createTireReference(this.tireReference).subscribe(
      (data) => {
        console.log(data);
        this.goToTireReferenceList();
      },
      (error) => console.log(error)
    );
  }

  addNew() {
    this.tireReference.batches.push(new Batch());
  }

  remove(index: number) {
    this.tireReference.batches.splice(index, 1);
  }

  goToTireReferenceList() {
    this.router.navigate(['/tireReferences']);
  }

  onSubmit() {
    console.log(this.tireReference);
    this.saveTireReference();
  }
}
