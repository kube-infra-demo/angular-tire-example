import { v4 as uuid } from 'uuid';

export class Batch {
  id: string = uuid();
  productionDate: Date;
  factoryLocation: string;
}
