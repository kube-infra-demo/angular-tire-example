import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TireReferenceListComponent } from './tireRefence-list/tireReference-list.component';
import { CreateTireReferenceComponent } from './create-tireReference/create-tireReference.component';
import { UpdateTireReferenceComponent } from './update-tireReference/update-tireReference.component';
import { TireReferenceDetailsComponent } from './tireReference-details/tireReference-details.component';

const routes: Routes = [
  { path: 'tireReferences', component: TireReferenceListComponent },
  { path: 'create-tireReference', component: CreateTireReferenceComponent },
  { path: '', redirectTo: 'tireReferences', pathMatch: 'full' },
  { path: 'update-tireReference/:id', component: UpdateTireReferenceComponent },
  {
    path: 'tireReference-details/:id',
    component: TireReferenceDetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
