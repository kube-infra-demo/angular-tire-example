### STAGE 1: Build ###
FROM node:14.15.5-alpine3.13 AS build
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
ARG backendUrl
RUN npm i -g @angular/cli@14

# Install app dependencies:
RUN npm i
RUN npm install webpack@~5.78.0

COPY . .
RUN ng build
### STAGE 2: Run ###
FROM nginx:1.17.1-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/src/app/dist/* /usr/share/nginx/html